 
cap log close
set linesize 250
version 14

clear all
set matsize 5000
set more off, permanently

set seed 66666666
set sortseed 66666666
set scheme s2mono

adopath + ..\external

program main
	explorative_figures_regressions 
end


program explorative_figures_regressions 
	use ../temp/preclean_agentTeamRoleHouse.dta, clear
	drop if agent_id == "0"
	drop if role_year == 1000 | role_year == 1970
	sort agent_id role_year role_month role_day
	g Tline = (role_year-2011)*12 + role_month
	bys agent_id: egen minTline = min(Tline)
	g Tline_normalized = Tline - minTline
	g yearInLianjian = role_year - join_year
	* sth might go wrong with the join year information...
	replace yearInLianjian = 0 if yearInLianjian<0
	bys housedel_id: egen max_yearInLianjian = max(yearInLianjian)
	bys housedel_id: egen mean_yearInLianjian = mean(yearInLianjian)
	bys housedel_id: egen numTeamMember = nvals(role_id)
	bys agent_id role_year role_month: egen numHouseAssigned = nvals(housedel_id)
	bys agent_id role_year role_month: egen numHouseAssignedFail = nvals(housedel_id) if status == "无效"
	bys agent_id: egen numTeamPerAgent = nvals(housedel_id)
	
	preserve
		bys agent_id:g n = _n
		drop if n~=1
		su numTeamPerAgent, detail
		mmerge agent_id using ../temp/coreAgentId.dta
		bys _m: su numTeamPerAgent, detail
	restore
	
	collapse (mean) numHouseAssigned numHouseAssignedFail max_yearInLianjian mean_yearInLianjian numTeamMember role_year role_month, by(agent_id Tline_normalized)
	
	g ratio = numHouseAssignedFail/numHouseAssigned
	replace ratio = 0 if ratio == .
	
	su Tline_normalized, detail
	tw lpolyci numHouseAssigned Tline_normalized if Tline_normalized<`r(p95)', legend(off)
	su Tline_normalized, detail
	tw lpolyci ratio Tline_normalized if Tline_normalized<`r(p95)', legend(off) 

	areg ratio max_yearInLianjian mean_yearInLianjian numTeamMember Tline_normalized i.role_year, absorb(agent_id)
end	
	
	
program explorative_figures_regressions_old
	use ../temp/agent_role_history.dta, clear
	sort agent_id role_time	
	sort agent_id role_time
	by agent_id: g switchRole = 1 if role_name ~= role_name[_n-1]
	replace switchRole = 0 if switchRole == .
	by agent_id: g cumuExperience = _n
	by agent_id: g N = _N
	g cumuExperience2 = cumuExperience*cumuExperience
	g cumuExperience3 = cumuExperience*cumuExperience*cumuExperience
	g initialRole = role_name if cumuExperience == 1
	g finalRole = role_name if cumuExperience == N
	by agent_id: replace initialRole = initialRole[_n-1] if initialRole == ""
	encode initialRole, g(initial_role)
	gsort agent_id -cumuExperience
	by agent_id: replace finalRole = finalRole[_n-1] if finalRole == ""
	tab initial_role, g(initial_role_dummy)
	reg switchRole cumuExperience cumuExperience2 initial_role_dummy*, noconstant
	bys housedel_id: egen cumuExperience_mean = mean(cumuExperience)
	bys housedel_id: egen cumuExperience_max = max(cumuExperience)
	bys housedel_id: egen cumuExperience_min = min(cumuExperience)
	bys housedel_id: g cumuExperience_diff = cumuExperience_max - cumuExperience_min
	
	mmerge agent_id using ../temp/agent_demo.dta, type(n:1) unmatched(none)
	mmerge housedel_id using ../temp/house_demo.dta, type(n:1) unmatched(master)
	mmerge agent_id using ../temp/agent_Info.dta, type(n:1) unmatched(none)
	*agent_id housedel_id house_id, it seems that house_id identifies trip
	*but unique house_id does not match total_shows
	*what is top_year 
	*status take two values "过户" "无效"
	*creat time, sign time
	encode store, g(Store)
	encode district, g(District)
	encode subway, g(subwayEncode) 
	encode school, g(schoolEncode)
	forvalues i = 1(1)16 {
	preserve
		bys agent_id housedel_id house_id: g nn = _n
		keep if nn == 1
		keep if District == `i'
		graph box cumuExperience_mean, over(Store) nooutsides ytitle("") name(district`i') nolabel
	restore
	}
	graph combine district1 district2 district3 district4 district5 district6 district7 district8 district9 district10 district11 district12 district13 district14 district15 district16
	graph export ../output/experience_hete_byDistrict.eps, replace
	graph box cumuExperience_mean, over(Store) by(District) nooutsides ytitle("") nolabel
	graph export ../output/experience_hete_byDistrict.pdf, replace
	graph drop _all 
	
	split create_time, p("/" " ")
	split sign_time, p("/" " ")
	split role_time, p("/" " ")
	destring role_time1 role_time2 sign_time1 sign_time3 create_time3 create_time1, replace force
	g sold_duration = ((sign_time3 - 2000)*12 + sign_time1) - ((create_time3 - 2000)*12 + create_time1)
	preserve
		bys housedel_id leader_id: egen experience_mean = mean(cumuExperience_mean)
		bys housedel_id leader_id: egen experience_gap = mean(cumuExperience_diff)
		bys housedel_id leader_id: g nn = _n
		bys housedel_id: egen numGroup = nvals(leader_id)
		keep if nn == 1
		keep if status == "过户"
		reg total_shows c.cumuExperience_mean##c.price i.District room livingroom kitchen bathroom floor no_price_adj subwayEncode schoolEncode numGroup
		reg total_shows c.experience_gap##c.price i.District room livingroom kitchen bathroom floor no_price_adj subwayEncode schoolEncode numGroup
		reg sold_duration total_shows c.cumuExperience_mean##c.price i.District room livingroom kitchen bathroom floor no_price_adj subwayEncode schoolEncode numGroup
		reg sold_duration total_shows c.experience_gap##c.price i.District room livingroom kitchen bathroom floor no_price_adj subwayEncode schoolEncode numGroup
		reg sold_duration c.cumuExperience_mean##c.price i.District room livingroom kitchen bathroom floor no_price_adj subwayEncode schoolEncode i.create_time3 numGroup
		reg sold_duration c.experience_gap##c.price i.District room livingroom kitchen bathroom floor no_price_adj subwayEncode schoolEncode i.create_time3 numGroup		
	restore 
end


program explore
	*agent_info.dta seems just a one shot agent_id-leader_id data
	use ../temp/Agent_Info.dta, clear
	* agent_sys_id agent_id leader_sys_id leader_id type org_structure company adm_region region store
	mmerge agent_id using ../temp/agent_demo.dta, type(n:1)
	
	use ../temp/Agent_Info.dta, clear
	mmerge agent_id using ../temp/tenure.dta, type(n:n)

	use ../temp/tenure.dta, clear
	
	
	use ../temp/agent_role_history.dta, clear
	mmerge agent_id using ../temp/Agent_Info.dta, unmatched(none)
	
	
	bys leader_sys_id: g n = _n
	keep if n == 1
	tab leader_N
	bys store: egen numAgent = nvals(agent_sys_id)

	use ../temp/tenure.dta, clear
	*9 agents have no information of change of position
	egen numAgent = nvals(agent_sys_id)
	
	use ../temp/agent_role_history.dta, clear
	bys agent_id: egen dealNum = nvals(housedel_id)
	egen numHouse =  nvals(housedel_id) 
	*105024 house
	bys agent_id: g n = _n
	keep if n==1
	su dealN, detail
	tab del_type
	
	use ../temp/agent_demo.dta, clear
	hist tenure_month
	su tenure_month, detail
end


main
