 
cap log close
set linesize 250
version 14

clear all
set matsize 5000
set more off, permanently

set seed 66666666
set sortseed 66666666
set scheme s2mono

adopath + ..\external

program main
	preclean
	mergeData
end


program preclean
	* 01Agent_Info.xlsx is not very useful. It serves as "seed id" at data collection stage 
	* data collection procedure can be described as:
	* 01 -> 04 -> 06 -> 09
	* 01 -> 03 -> 05 -> 07 
	* 08 似乎拼不上
	* 09 是怎么collect的？感觉有问题，04里面有的agent, 09里缺失。09里面有然而04没有是合理的。初步怀疑09 collection 有问题。
	* 10 and 02 can be combined
	* 04Agent_role_house.csv

	import excel "..\external\01Agent_Info.xlsx", sheet("sheet1") firstrow clear
	destring agent_sys_id leader_sys_id, replace
	bys agent_sys_id: g agent_N = _N
	bys leader_sys_id: g leader_N = _N 
	bys agent_id: g n = _n
	drop if n == 2
	drop n
	g isCoreAgent = 1
	rename (company adm_region region store) (company_coreAgent adm_region_coreAgent region_coreAgent store_coreAgent)
	save ../temp/coreAgentId.dta, replace
	
	import delimited "..\external\04Agent_role_house.csv", varnames(1) stringcols(2 3 5 7 9 11 13) encoding(GB18030) clear 
	*creator 录入人,  holder 维持人, fast 速销人, sign 成交人
	rename (ucid_house_creator role_time_creator ucid_house_holder role_time_holder ucid_house_photo role_time_photo ucid_house_key role_time_house_key ucid_house_fast role_time_fast ucid_house_sign role_time_sign) ///
	(agent_id1 role_time1 agent_id2 role_time2 agent_id3 role_time3 agent_id4 role_time4 agent_id5 role_time5 agent_id6 role_time6)
	g role_type1 = 1 if agent_id1 ~= "0"
	g role_type2 = 2 if agent_id2 ~= "0"
	g role_type3 = 3 if agent_id3 ~= "0"
	g role_type4 = 4 if agent_id4 ~= "0"
	g role_type5 = 5 if agent_id5 ~= "0"
	g role_type6 = 6 if agent_id6 ~= "0"
	drop id
	reshape long agent_id role_time role_type, i(housedel_id) j(role_id) 
	replace role_time = "" if agent_id == "0"
	split role_time, p("/" " ")
	rename (role_time1 role_time2 role_time3) (role_year role_month role_day)
	drop role_time4 role_time
	destring role_year role_month role_day, replace 
	save ../temp/agentTeamRoleHouse.dta, replace
	
	import excel "..\external\06House_Demo.xlsx", sheet("sheet1") firstrow allstring clear
	destring no_price_adj total_shows price size house_grade room livingroom kitchen bathroom management_fee floor total_floor top_year latitude longitude, replace force
	g isBeijing = 1 if district == "东城" | district == "西城" | district == "朝阳" | district == "丰台" | district == "石景山" | ///
	district == "海淀" | district == "门头沟" | district == "房山" | district == "大兴" | district == "通州" | district == "顺义" | ///
	district == "昌平" | district == "怀柔" | district =="平谷" | district == "密云" | district == "延庆"	
	*only keep the houses in 16 main districts in beijing 
	keep if isBeijing == 1
	*drop multi-customer id observation
	bys housedel_id: g n = _n
	keep if n == 1
	save ../temp/houseDemo.dta, replace
		
	import delimited "..\external\09house_show.csv", bindquote(strict) varnames(1) stringcols(1 2 3) clear 
	*ucid 是带看人，陪看人，buyer side data (?但是带看人里面也会出现agent_id)
	save ../temp/house_show.dta, replace 
	
	* RA mentioned that she uses house_id cust_id ucid to identify a unique trip. *
	* 03 file may also tell us how many times a customer visited a certain house or used lianjian service.
	* house_id 是什么东西? 还是不太确定
	import delimited "..\external\03Agent_role_history.txt", varnames(1) stringcols(2 4) encoding(GB18030) clear 
	split role_time, p("/" " ")
	rename (role_time1 role_time2 role_time3) (role_year role_month role_day)
	drop role_time4 role_time
	destring role_year role_month role_day, replace 
	save ../temp/agent_role_history.dta, replace	
 
	*05 可以和 03 merge. 05 里面太多string variable, 需要时候再merge
	*05 可能可以用来confirm core agent 的抽取是否有bias
  	import delimited "..\external\05Agent_Demo.csv", bindquote(strict) stringcols(1) varnames(1) encoding(GB18030) clear 
	split join_date1, p("/")
	rename (join_date11 join_date12) (join_year join_month)
	drop join_date1 join_date13 join_date14 join_date15
	destring join_year join_month, replace force
	replace join_year = . if join_year<2000
	replace join_month = . if join_year<2000
	mmerge agent_id using ../temp/coreAgentId.dta
	save ../temp/agent_demo.dta, replace

	*07 可能可以用来检查sampling bias
	import delimited "..\external\07Agent_performance.csv", bindquote(strict) varnames(1) stringcols(1) clear 
	tostring yearmont, replace
	g role_year = substr(yearmont,1,4)
	g role_month = substr(yearmont,5,2)
	destring role_*, replace
	drop yearmont
	drop if role_year == .
	save ../temp/agent_performance.dta, replace
	
	import delimited "..\external\08Price_Adjustment.csv", bindquote(strict) varnames(1) stringcols(1) clear 
	save ../temp/price_Adjustment.dta, replace	
	
	import excel "..\external\02Agent_tenure_history.xlsx", sheet("sheet1") firstrow clear
	keep agent_sys_id leader_sys_id agent_id change_type position change_time
	destring agent_sys_id leader_sys_id, replace
	*here we drop org_structure company adm_region region store team
	drop if position == "null"
	split change_time, p("-")
	rename (change_time1 change_time2) (change_time_year change_time_month)
	drop change_time change_time3
	save ../temp/tenure.dta, replace
	
	import delimited "..\external\10Related_Agent_Tenure_History.csv", bindquote(strict) varnames(1) encoding(GB18030) stringcols(1) clear 
	drop if position == "null"
	split change_time, p("-")
	rename (change_time1 change_time2) (change_time_year change_time_month)
	drop change_time change_time3
	save ../temp/related_agent_tenure_history.dta, replace
end


program mergeData
	use ../temp/agent_demo.dta, clear
	keep agent_id position_type company adm_region region team store
	save ../temp/agent_demo_temp.dta, replace
	
	*only keep beijing lianjian
	use ../temp/houseDemo.dta, clear
	keep housedel_id status biz_zone
	*5534 house only in house demo data, 103318 houses both in master and using data 
	mmerge housedel_id using ../temp/agentTeamRoleHouse.dta, type(1:n) unmatched(none) 
	order housedel_id agent_id role_id, first
	mmerge agent_id using ../temp/agent_demo_temp, type(n:1) unmatched(master)
	sort housedel_id role_id agent_id 
	by housedel_id: replace role_year= role_year[_n-1] if role_year[_n] == . & role_year[_n-1] ~=.
	by housedel_id: replace role_month= role_month[_n-1] if role_month[_n] == . & role_month[_n-1] ~=.	
	mmerge agent_id using ../temp/agent_demo.dta, type(n:1) unmatched(master)
	mmerge agent_id using ../temp/coreAgentId.dta, type(n:1) unmatched(master)
	save ../temp/preclean_agentTeamRoleHouse.dta, replace

	use ../temp/preclean_agentTeamRoleHouse.dta, clear
	mmerge agent_id role_year role_month using ../temp/agent_performance.dta, type(n:1) unmatched(master)
	order housedel_id agent_id role_id biz_zone status role_year role_month role_day sell rent, first
	sort housedel_id role_id agent_id 
	save ../temp/preclean_agentTeamRoleHouse_withPerformace.dta, replace
	
	use ../temp/tenure.dta, clear
	append using ../temp/related_agent_tenure_history.dta 
	drop agent_sys_id leader_sys_id
	sort agent_id change_time_year change_time_month
	duplicates drop
	save ../temp/tenure_combine_uncleaned.dta, replace
end


main
