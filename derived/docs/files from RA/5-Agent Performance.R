
##all_perf
load("all_perf.RData")
# {
# all_perf = data.frame(fread("07Agent_performance.csv",integer64 = "character"))
# agent_all_char = unique(c(all_char$ucid_house_creator,
#                             all_char$ucid_house_holder,
#                             all_char$ucid_house_photo,
#                             all_char$ucid_house_key,
#                             all_char$ucid_house_fast,
#                             all_char$ucid_house_sign))
# agent_all = unique(c(agent_all_char,house_show$ucid))
# 
#   all_perf = all_perf%>%
#   filter(agent_id%in%agent_all)%>%
#   data.frame()
# # length(unique(all_perf$agent_id))
# #   # [1] 46825
# # setdiff(unique(all_perf$agent_id),agent_all)
# # agent_without_perf = setdiff(agent_all,all_perf$agent_id)
# # # length(setdiff(agent_all,all_perf$agent_id))
# # # [1] 2425
# # length(unique(house_show[house_show$ucid%in%
# #                            agent_without_perf,]$housedel_id))
# # # [1] 6500
# # house_without_agent_perf = all_char%>%
# #   select(housedel_id,starts_with("ucid"))%>%
# #   melt(id = "housedel_id")%>%
# #   filter(value %in% setdiff(agent_without_perf,"0"))%>%
# #   data.frame()
# # length(unique(house_without_agent_perf$housedel_id))
# # # [1] 3014
# # house_without_agent_perf = union(house_without_agent_perf$housedel_id,unique(house_show[house_show$ucid%in%
# #                                                                    agent_without_perf,]$housedel_id))
# 
# #remove na yearmont
# all_perf$yearmont = as.numeric(all_perf$yearmont)
# all_perf = all_perf%>%
#   filter(!is.na(yearmont)&yearmont!="300009")%>%
#   data.frame()
# #adjust format of yearmont
# all_perf$yearmont = paste(str_sub(all_perf$yearmont,1,4),
#                    str_sub(all_perf$yearmont,-2,-1),
#                    "01",
#                    sep = "-")
# all_perf$yearmont = as.Date(all_perf$yearmont)
# all_perf = all_perf[!is.na(all_perf$yearmont),]
# 
# agent_perf = unique(all_perf$agent_id)
# 
# count_agent_perf = all_perf%>%
#   group_by(yearmont)%>%
#   summarise(count = n())%>%
#   data.frame()
# 
# YearMont = sort(unique(all_perf$yearmont))[-c(1:6)]
# YearMont = YearMont[-c((length(YearMont)-7):length(YearMont))]
# index_perf = data.frame(agent_id = as.character(sort(rep(agent_perf,5+11+12*4))),
#                         yearmont = rep(YearMont,length(agent_perf)))
# 
# all_perf = all_perf%>%
#   filter(yearmont%in%YearMont)%>%
#   data.frame()%>%
#   full_join(index_perf,by = c("agent_id","yearmont"))%>%
#   arrange(agent_id,yearmont)
# 
# compute_perf = function(x, lag = 3){
#   tmp = data.frame(matrix(NA,nrow = length(x), ncol = lag))
#   n = length(x)
#   m = n/64
#   for(i in lag:1){
#     tmp[,i] = c(rep(NA,i),x[1:(n-i)])
#     tmp[64*(1:(m-1))+i,i:lag] = NA
#   }
#   re = apply(tmp,1,function(y)round(mean(y,na.rm = T),2))
#   re[is.nan(re)] = NA
#   return(re)
# }
# 
# index = data.frame(var = c("sell_mon","sell_quarter","sell_half","sell_year",
#                            "rent_mon","rent_quarter","rent_half","rent_year"),
#                    lagsize = rep(c(1,3,6,12),2))
# index$var = as.character(index$var)
# 
# for(i in 1:nrow(index)){
#   cat(i)
#   cat("\n")
#   if(str_detect(index$var[i],"sell")){
#     all_perf[,index$var[i]] = compute_perf(all_perf$sell,lag = index$lagsize[i])
#   }else{
#     all_perf[,index$var[i]] = compute_perf(all_perf$rent,lag = index$lagsize[i])
#   }
# }
# for(i in 3:ncol(all_perf)){
#   all_perf[is.na(all_perf[,i]),i] = 0
# }
# all_perf = all_perf%>%
#   group_by(agent_id)%>%
#   mutate(hist_sell = c(0,cumsum(sell)[-n()]),
#          hist_rent = c(0,cumsum(rent)[-n()]))
# rm(agent_all_char,agent_perf,bj,agent_without_house,house_without_info,
#    lat,long,YearMont,compute_perf,index,index_perf,house_info_rent)
# }
# save(all_perf,file = "all_perf.RData")
# 